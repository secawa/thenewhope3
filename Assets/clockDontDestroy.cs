﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class clockDontDestroy : MonoBehaviour
{
    private static clockDontDestroy _instance = null;

    public static clockDontDestroy Instance
    {
        get { return _instance; }
    }

    public static int menuScreenBuildIndex; //the menu screen's index in your Build Settings

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(transform.root.gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(transform.root.gameObject);

        SceneManager.activeSceneChanged += DestroyOnMenuScreen;
    }

    void DestroyOnMenuScreen(Scene oldScene, Scene newScene)
    {
        if (newScene.buildIndex == menuScreenBuildIndex) //could compare Scene.name instead
        {
            Destroy(this); //change as appropriate
        }
    }
}