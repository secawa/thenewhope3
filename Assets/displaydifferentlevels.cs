using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.XR.WSA.Input;
using UnityEngine.UI;

namespace HoloToolkit.Unity
{
    public class displaydifferentlevels : MonoBehaviour {



        public bool onlyonce = true;
        public bool onlyonce1 = true;
        public GameObject[] canvas;
        public GameObject thecamera;
        public GameObject walls;

        private class ControllerState
        {
            public InteractionSourceHandedness Handedness;
            public bool Grasped;
            public bool MenuPressed;
            public bool TouchpadPressed;
            public bool TouchpadTouched;
            public Vector2 TouchpadPosition;

        }

        private Dictionary<uint, ControllerState> controllers;

        // Text display label game objects

        private void Awake()
        {
            thecamera = GameObject.Find("MixedRealityCameraParent");
            controllers = new Dictionary<uint, ControllerState>();
            InteractionManager.InteractionSourceDetected += InteractionManager_InteractionSourceDetected;

            InteractionManager.InteractionSourceLost += InteractionManager_InteractionSourceLost;
            InteractionManager.InteractionSourceUpdated += InteractionManager_InteractionSourceUpdated;
        }

        private void Start()
        {
            if (DebugPanel.Instance != null)
            {
                DebugPanel.Instance.RegisterExternalLogCallback(GetControllerInfo);
            }
        }
        private void Update()
        {
            if (!thecamera.GetComponent<Collider>().bounds.Intersects(walls.GetComponent<Collider>().bounds))
            {
                thecamera.transform.position = new Vector3(0,0,0);
            }
            foreach (ControllerState controllerState in controllers.Values)
            {
                thecamera.transform.position += new Vector3(controllerState.TouchpadPosition.x * Time.deltaTime,0, controllerState.TouchpadPosition.y * Time.deltaTime);
                if (controllerState.Handedness.Equals(InteractionSourceHandedness.Left))
                {
                    if (controllerState.MenuPressed == true && onlyonce == true)
                    {
                        panelOpen();
                        onlyonce = false;
                    }
                    else if (controllerState.MenuPressed == false)
                    {
                        onlyonce = true;
                    }
                }
                if (controllerState.Handedness.Equals(InteractionSourceHandedness.Right))
                {
                    if (controllerState.MenuPressed == true && onlyonce1 == true)
                    {
                        panelOpen();
                        onlyonce1 = false;
                    }
                    else if (controllerState.MenuPressed == false)
                    {
                        onlyonce1 = true;
                    }
                }


                

            }

        }
        private void InteractionManager_InteractionSourceDetected(InteractionSourceDetectedEventArgs obj)
        {
            Debug.LogFormat("{0} {1} Detected", obj.state.source.handedness, obj.state.source.kind);

            if (obj.state.source.kind == InteractionSourceKind.Controller && !controllers.ContainsKey(obj.state.source.id))
            {
                controllers.Add(obj.state.source.id, new ControllerState { Handedness = obj.state.source.handedness });
            }
        }

        private void InteractionManager_InteractionSourceLost(InteractionSourceLostEventArgs obj)
        {
            Debug.LogFormat("{0} {1} Lost", obj.state.source.handedness, obj.state.source.kind);

            controllers.Remove(obj.state.source.id);
        }

        private void InteractionManager_InteractionSourceUpdated(InteractionSourceUpdatedEventArgs obj)
        {
            ControllerState controllerState;
            if (controllers.TryGetValue(obj.state.source.id, out controllerState))
            {

                controllerState.MenuPressed = obj.state.menuPressed;
                controllerState.TouchpadPressed = obj.state.touchpadPressed;
                controllerState.TouchpadTouched = obj.state.touchpadTouched;
                controllerState.TouchpadPosition = obj.state.touchpadPosition;

            }
        }

        private string GetControllerInfo()
        {

            string toReturn = string.Empty;
            foreach (ControllerState controllerState in controllers.Values)
            {
                //  Debug.Log(controllerState.TouchpadPosition);
                // Debug message
                toReturn += string.Format("Hand: {0}",
                                          "MenuPressed: {6} Touchpad: Pressed: {11}" + "Touched: {12} Position: {13}\n",
                                          controllerState.Handedness,
                                          controllerState.MenuPressed, controllerState.TouchpadPressed,
                                          controllerState.TouchpadTouched, controllerState.TouchpadPosition);
                //Debug.Log(controllerState.MenuPressed);
                // Text label display       


            }
            return toReturn.Substring(0, Math.Max(0, toReturn.Length - 2));
        }

        public void panelOpen()
        {
            if (canvas[0].activeSelf == false)
            {
                canvas[0].SetActive(true);
                
            }
            else
            {
                canvas[0].SetActive(false);
            }

        }
    }
}