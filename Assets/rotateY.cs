using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace HoloToolkit.Unity.Examples
{
    public class rotateY : MonoBehaviour {
    public float rotorspeed;
        public float thespeed = 20f;
    public GameObject heli;
        public bool reachedheight = false;
        public bool firstpoint;
        public bool secondpoint;
        public GameObject[] Locations;
    public GameObject scripts;
        public int number;
	// Use this for initialization
	void Start () {
            secondpoint = false;
            scripts = GameObject.Find("TheScripts");
            for (int i = 0; i < Locations.Length; i++)
        {
            Locations[i] = GameObject.Find("3D Buildings/" + scripts.GetComponent<FindThebuildingsagain>().namesoflocations[i]+ "/Teleport");
        }
    }

        // Update is called once per frame
        void Update()
        {
            this.gameObject.transform.Rotate(0, rotorspeed * Time.deltaTime, 0);
            if (rotorspeed <= 1000)
            {
                rotorspeed += 40 * Time.deltaTime;
            }
            else if (rotorspeed >= 1000 && heli.transform.position.y <= 100)
            {
                heli.transform.position += new Vector3(0, 4 * Time.deltaTime, 0);
            }
            if (heli.transform.position.y >= 100 && reachedheight == false)
            {
                firstpoint = true;
                reachedheight = true;
            }
            if (firstpoint == true)
            {
                if (Vector3.Distance(heli.transform.position, Locations[number].transform.position) <= 2f && secondpoint == false)
                {
                    number++;
                    secondpoint = true;
                    if (number == Locations.Length)
                    {
                        number = 0;
                    }
                }
                else
                {
                    secondpoint = false;
                }
                heli.transform.rotation = Quaternion.Lerp(heli.transform.rotation, Quaternion.LookRotation(Locations[number].transform.position), Time.deltaTime);
                heli.transform.position = Vector3.MoveTowards(heli.transform.position, Locations[number].transform.position, thespeed * Time.deltaTime);
                heli.transform.Rotate(Vector3.Distance(heli.transform.position, Locations[number].transform.position) / 100 * Time.deltaTime, 0, 0);
            }
        }
	}
}
